import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'morecat';

  inputArray = [];
  inputForView = [];

  constructor() {
    // Init Bouchon
    for (let i = 1; i < 200; i++) {
      this.inputArray.push('chat : ' + i);
    }
  }

  ngOnInit() {
    this.inputForView = this.inputForView.concat(this.inputArray.slice(0, 10));
  }

  moreCat() {
    const beginIndex = this.inputForView.length;
    this.inputForView = this.inputForView.concat(this.inputArray.slice(beginIndex, beginIndex + 10));
  }
}
